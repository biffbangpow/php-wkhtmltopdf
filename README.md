# WKHTMLTOPDF & PHP Docker container

Using [PHP 7.1 FPM](https://hub.docker.com/_/php/) as a parent this provides a
container that also has the wkhtmltopdf installed.

## Building

    docker build --force-rm -t bbpdev/php-wkhtmltopdf:<tag> .

## Pushing

    docker login
    docker push bbpdev/php-wkhtmltopdf:<tag> .

## Versions

As of time of writing this is using the following Versions

* PHP 7.1
* WKHTMLTOPDF 0.12.4
